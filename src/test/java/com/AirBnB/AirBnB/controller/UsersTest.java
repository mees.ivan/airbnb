package com.AirBnB.AirBnB.controller;

import com.AirBnB.AirBnB.model.Users;
import com.AirBnB.AirBnB.repository.UsersRepository;
import com.AirBnB.AirBnB.service.UsersService;
import com.AirBnB.AirBnB.util.TipoUsers;
import jdk.swing.interop.SwingInterOpUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


public class UsersTest {

    private UsersService usersService;

    private UsersRepository usersRepository;

    @BeforeEach
    public void setUP() {
        usersRepository = mock(UsersRepository.class);
        usersService = new UsersService(usersRepository, null);
    }


    @Test
    public void testSalvar() throws Exception {

        Users user = new Users();
        user.setUserId(1L);
        user.setNome("ivanmees");
        user.setSenha("senha123");
        user.setTipoUser(TipoUsers.Locatario);
        System.out.println(user);

        when(usersRepository.findAll()).thenReturn(new ArrayList<>());
        when(usersRepository.insert(any(Users.class))).thenReturn(user);

        ResponseEntity<Users> aux = usersService.salvaUser(user);

        assertEquals(aux.getBody(), user);
    }

    @Test
    public void testSalvarUserJaCriado(){
        Users user = new Users();
        user.setUserId(1L);
        user.setNome("ivanmees");
        user.setSenha("senha123");
        user.setTipoUser(TipoUsers.Locatario);
        System.out.println(user);

        ArrayList<Users> users = new ArrayList<>();
        users.add(user);

        when(usersRepository.findAll()).thenReturn(users);

        ResponseEntity<Users> aux = usersService.salvaUser(user);

        assertEquals(ResponseEntity.status(HttpStatus.FOUND).build(), aux);
    }


}
