package com.AirBnB.AirBnB.model;

import com.AirBnB.AirBnB.util.TipoImovel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@NoArgsConstructor
public class Imovel {
    @Id
    private Long id;
    private String nome;
    private TipoImovel tipoImovel;
    private String cidade;
    private Double diaria;
    private Boolean disponivel = true;
}
