package com.AirBnB.AirBnB.model;

import com.AirBnB.AirBnB.util.TipoUsers;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.ArrayList;
import java.util.Objects;

@Component()
@Data
@NoArgsConstructor
public class Users {
    @Id()
    private Long userId;
    private String nome;
    private String senha;
    private TipoUsers tipoUser;
    private Boolean logado = false;
    ArrayList<Imovel> imoveis = new ArrayList<>();

    public void setImoveis(Imovel imovel) {
        this.imoveis.add(imovel);
    }

    public Boolean removeImoveis(Imovel imovel) {

        for(Imovel imovel1: imoveis){
            if(Objects.equals(imovel1.getId(), imovel.getId())){
                imoveis.remove(imovel1);
                return true;
            }
        }
        return false;
    }

}
