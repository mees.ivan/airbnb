package com.AirBnB.AirBnB.repository;

import com.AirBnB.AirBnB.model.Imovel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImovelRepository extends MongoRepository<Imovel, Long> {
}
