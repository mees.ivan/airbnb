package com.AirBnB.AirBnB.repository;

import com.AirBnB.AirBnB.model.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UsersRepository extends MongoRepository<Users, Long> {
}
