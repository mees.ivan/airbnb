package com.AirBnB.AirBnB.controller;

import com.AirBnB.AirBnB.model.Imovel;
import com.AirBnB.AirBnB.util.TipoImovel;
import com.AirBnB.AirBnB.service.ImovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/Imoveis")
@RestController
public class ImovelController {

    @Autowired ImovelService imovelService;

    @PostMapping("/CadatrarImovel/{nome}")
    public ResponseEntity<Imovel> cadastrarImovel(@PathVariable String nome, @RequestBody Imovel imovel){
        return imovelService.salvaImovel(nome, imovel);
    }

    @GetMapping("/BuscarImovel")
    public ResponseEntity<List<Imovel>> listAll(){
        return imovelService.listAll();
    }

    @GetMapping("/BuscarImovelPorTipo/{tipoImovel}")
    public ResponseEntity<List<Imovel>> listAllByType(@PathVariable TipoImovel tipoImovel){
        return imovelService.listAllByType(tipoImovel);
    }

}
