package com.AirBnB.AirBnB.controller;

import com.AirBnB.AirBnB.model.Imovel;
import com.AirBnB.AirBnB.model.Users;
import com.AirBnB.AirBnB.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/Users")
@RestController
public class UsersController {
    @Autowired UsersService usersService;

    @PostMapping("/Salvar")
    public ResponseEntity<Users> salvaUser(@RequestBody Users users){
        return usersService.salvaUser(users);
    }

    @PutMapping("/AlterarNome/{nome}")
    public ResponseEntity<Users> alteraUserNome(@RequestBody Users novoNome, @PathVariable String nome){
        return usersService.alteraUserNome(novoNome, nome);
    }

    @PutMapping("/AlterarSenha/{nome}")
    public ResponseEntity<Users> alteraUserSenha(@RequestBody Users novaSenha, @PathVariable String nome){
        return usersService.alteraUserSenha(novaSenha,nome);
    }

    @PostMapping("/Logar")
    public ResponseEntity<String> logarUser(@RequestBody Map<String, String> json){
        return usersService.logarUser(json.get("nome"),json.get("senha"));
    }

    @PostMapping("/Deslogar")
    public ResponseEntity<String> deslogarUser(@RequestBody String nome){
        return usersService.deslogarUser(nome);
    }

    @GetMapping
    public List<Users> listAll(){
        return usersService.listAll();
    }

    @PostMapping("/Alugar/{nome}")
    public ResponseEntity<Users> alugaImovel(@PathVariable String nome, @RequestBody Imovel imovel){
        return usersService.alugaImovel(nome,imovel);
    }

    @PostMapping("/Desalugar/{nome}")
    public ResponseEntity<Users> desalugarImovel(@PathVariable String nome, @RequestBody Imovel imovel){
        return usersService.desalugaImovel(nome,imovel);
    }
}
