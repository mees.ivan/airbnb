package com.AirBnB.AirBnB.service;

import com.AirBnB.AirBnB.model.Imovel;
import com.AirBnB.AirBnB.util.TipoImovel;
import com.AirBnB.AirBnB.util.TipoUsers;
import com.AirBnB.AirBnB.model.Users;
import com.AirBnB.AirBnB.repository.ImovelRepository;
import com.AirBnB.AirBnB.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImovelService {
    @Autowired ImovelRepository imovelRepository;
    @Autowired UsersService usersService;
    @Autowired UsersRepository usersRepository;

    public ResponseEntity<Imovel> salvaImovel(String nome, Imovel imovel){
        Users users = usersService.validaUser(nome);

        if(users.getLogado() && users.getTipoUser().equals(TipoUsers.Locador)){
            imovelRepository.insert(imovel);
            users.setImoveis(imovel);
            usersRepository.save(users);
            return ResponseEntity.status(HttpStatus.CREATED).body(imovel);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    public ResponseEntity<List<Imovel>> listAll(){
        List<Imovel> imoveisDisponiveis = new ArrayList<>();

        imovelRepository.findAll().forEach(imovel -> {
            if(imovel.getDisponivel()){
                imoveisDisponiveis.add(imovel);
            }
        });

        return ResponseEntity.status(HttpStatus.FOUND).body(imoveisDisponiveis);
    }

    public ResponseEntity<List<Imovel>> listAllByType(TipoImovel tipo){
        List<Imovel> imoveisDisponiveis = new ArrayList<>();

        imovelRepository.findAll().forEach(imovel -> {
            if((imovel.getDisponivel()) && imovel.getTipoImovel() == tipo){
                imoveisDisponiveis.add(imovel);
            }
        });

        return ResponseEntity.status(HttpStatus.FOUND).body(imoveisDisponiveis);
    }




}
