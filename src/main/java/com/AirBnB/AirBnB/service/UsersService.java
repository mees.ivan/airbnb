package com.AirBnB.AirBnB.service;

import com.AirBnB.AirBnB.model.Imovel;
import com.AirBnB.AirBnB.util.TipoUsers;
import com.AirBnB.AirBnB.model.Users;
import com.AirBnB.AirBnB.repository.ImovelRepository;
import com.AirBnB.AirBnB.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class UsersService {

    @Autowired UsersRepository usersRepository;

    @Autowired ImovelRepository imovelRepository;

    public ResponseEntity<Users> salvaUser(Users users){
        if(validaUser(users.getNome()) != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(usersRepository.insert(users));
        }
        return ResponseEntity.status(HttpStatus.FOUND).build();
    }

    public ResponseEntity<Users> alteraUserNome(Users users, String nome){
        Users user = validaUser(nome);
        if(user != null){
            System.out.println(users.getNome());
            user.setNome(users.getNome());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(usersRepository.save(user));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public ResponseEntity<Users> alteraUserSenha(Users users, String nome){
        Users user = validaUser(nome);
        if(user != null){
            user.setSenha(users.getSenha());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(usersRepository.save(user));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    public List<Users> listAll(){
        return usersRepository.findAll();
    }

    public ResponseEntity<String> logarUser(String nome, String senha){
        Users users = validaUser(nome);
        if(users.getSenha().equals(senha)){
            users.setLogado(true);
            usersRepository.save(users);
            return ResponseEntity.ok().body("Logado com sucesso!!!");
        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<String> deslogarUser(String nome){
        Users users = validaUser(nome);
        if(!users.getLogado()){
            users.setLogado(false);
            usersRepository.save(users);
            return ResponseEntity.ok().body("Deslogado com sucesso!!!");
        }
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Users> alugaImovel(String nome, Imovel imovel){
        Users user = validaUser(nome);

        if(user.getTipoUser() == TipoUsers.Locatario){
            if(imovelRepository.existsById(imovel.getId())){
                if(imovel.getDisponivel()){
                    user.setImoveis(imovel);
                    usersRepository.save(user);
                    imovel.setDisponivel(false);
                    imovelRepository.save(imovel);
                    return ResponseEntity.ok().body(user);
                }
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    public ResponseEntity<Users> desalugaImovel(String nome, Imovel imovel){
        Users user = validaUser(nome);

        if(user.removeImoveis(imovel)){
            usersRepository.save(user);
            imovel.setDisponivel(true);
            imovelRepository.save(imovel);
            return ResponseEntity.ok().body(user);
         };

        return ResponseEntity.notFound().build();
    }

    public Users validaUser(String nome){

        Users users = new Users();

        for(Users user: usersRepository.findAll()){
            System.out.println(user.getNome());
            if(user.getNome() != null){
                if(user.getNome().equals(nome)){
                    users = user;
                }
            }
        }
        return null;
    }
}
